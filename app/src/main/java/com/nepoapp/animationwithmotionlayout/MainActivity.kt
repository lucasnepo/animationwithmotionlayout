package com.nepoapp.animationwithmotionlayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        motion()
    }

    private fun motion(){
        val listener = AppBarLayout.OnOffsetChangedListener { _, verticalOffSet ->
            val seekPosition = -verticalOffSet / appbarlayout.totalScrollRange.toFloat()
            motion_layout.progress = seekPosition
        }

        appbarlayout.addOnOffsetChangedListener(listener)
    }

}
